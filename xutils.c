/* 
 * Copyright (C) 1996-1998 Szeredi Miklos
 * Email: mszeredi@inf.bme.hu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. See the file COPYING. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/* #define DEBUG_COLOR */

#include "xscr.h"
#include "spscr.h"
#include "spscr_p.h"
#include "ax.h"
#include "xdispkb.h"
#include "spperif.h"
#include "interf.h"
#include "spconf_p.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>

#include <X11/Xutil.h>
#include <X11/Xatom.h>

#define MAX_SEARCH_DEPTH 8


int privatemap = 0;

static unsigned cdist(XColor *c1, XColor *c2)
{
  int dr, dg, db;

  dr = (c1->red - c2->red) >> 8;
  dg = (c1->green - c2->green) >> 8;
  db =  (c1->blue - c2->blue) >> 8;

  return dr * dr + dg * dg + db * db;
}

static int already_alloced(pixt *colors, int num, pixt pix)
{
  int i;

  for(i = 0; i < num; i++) if(colors[i] == pix) return 1;
  return 0;
}

#define MYINF ((unsigned) -1)

static int search_colors(Colormap cmap, unsigned dp)
{
  int c;
  XColor xcolor, tmpcolor;
  pixt pix;

  XColor *scrcolors = NULL;
  unsigned  ncolors = 0;
  unsigned dist, mindist;
  pixt minpix;
  int failed = 0, lfailed;
  int fails[COLORNUM];

  if(dp < 4) failed = 1;
  if(!failed) {
    if(dp <= MAX_SEARCH_DEPTH) {
      ncolors = 1 << dp;
      scrcolors = malloc(sizeof(XColor) * ncolors);

      if(scrcolors != NULL) {
	for(pix = 0; pix < ncolors; pix++) scrcolors[pix].pixel = pix;
	XQueryColors(xsp_disp, cmap, scrcolors, (int) ncolors);
      }
    }
    
    
    for(c = 0; c < COLORNUM; c++) {
      xcolor.red = spscr_crgb[c].r << 10;
      xcolor.green = spscr_crgb[c].g << 10;
      xcolor.blue = spscr_crgb[c].b << 10;
      lfailed = 0;
      if(!XAllocColor(xsp_disp, cmap, &xcolor) || 
         already_alloced(xsp_colors, c, xcolor.pixel)) {
        
        if(scrcolors != NULL) {
#ifdef DEBUG_COLOR
          fprintf(stderr, "XAllocColor failed, searching all colors...\n");
#endif
          mindist = MYINF;
          
          for(pix = 0; pix < ncolors; pix++) {
            dist = cdist(&scrcolors[pix], &xcolor);
            if(dist < mindist) {
              tmpcolor.red = scrcolors[pix].red;
              tmpcolor.green = scrcolors[pix].green;
              tmpcolor.blue = scrcolors[pix].blue;
              if(XAllocColor(xsp_disp, cmap, &tmpcolor) &&
                 !already_alloced(xsp_colors, c, tmpcolor.pixel)) {
                if(mindist != MYINF) 
                  XFreeColors(xsp_disp, cmap, &minpix, 1, 0);
                mindist = dist;
                minpix = pix;
              }
            }
          }
          
          if(mindist == MYINF) {
            lfailed = 1;
#ifdef DEBUG_COLOR
            fprintf(stderr, "Search failed\n");
#endif
          }
          else xsp_colors[c] = minpix;
        }
        else {
#ifdef DEBUG_COLOR  
          fprintf(stderr, "XAllocColor failed\n");
#endif
          lfailed = 1;
        }
      }
      else xsp_colors[c] = xcolor.pixel;
      
      if(lfailed) {
	fails[c] = 1;
	failed = 1;
      }
      else fails[c] = 0;
      
      
#ifdef DEBUG_COLOR
      tmpcolor.pixel = xsp_colors[c];
      XQueryColor(xsp_disp, cmap, &tmpcolor);
      
      
      fprintf(stderr, 
              "Color %2i (%3i %3i %3i) -> Pixel %5lu (%3i %3i %3i)\n",
              c, spscr_crgb[c].r, spscr_crgb[c].g, spscr_crgb[c].b,
              xsp_colors[c], tmpcolor.red>>10, tmpcolor.green>>10, 
              tmpcolor.blue>>10);
#endif    
      
    }
    if(failed) {
      for(c = 0; c < COLORNUM; c++) {
	if(!fails[c]) XFreeColors(xsp_disp, cmap, &xsp_colors[c], 1, 0);
	xsp_colors[c] = c;
      }
    }
  }
  if(scrcolors != NULL) free(scrcolors);

  if(failed) return 0;
  return 1;
}

static int firstalloced = 1;
static int currprivate;

int allocate_colors(void)
{
  int color_res;
  Colormap defcmap;

  if(!firstalloced && !currprivate) 
    XFreeColors(xsp_disp, xsp_cmap, xsp_colors, COLORNUM, 0);
  
  defcmap = DefaultColormap(xsp_disp, xsp_scr);
  color_res = search_colors(defcmap, xsp_depth);

  if((!color_res || privatemap) && 
     (xsp_visual->class == PseudoColor || xsp_visual->class == GrayScale)) {
    XColor xcolor;
    int c;
    
    if(color_res) XFreeColors(xsp_disp, defcmap, xsp_colors, COLORNUM, 0);

    if(firstalloced || !currprivate) 
      xsp_cmap = XCreateColormap(xsp_disp, xsp_win, xsp_visual, AllocAll);

    if(xsp_depth <= MAX_SEARCH_DEPTH) {   /* Is this good? */
      for(c = 0; c < (1 << xsp_depth); c++) {
	xcolor.pixel = c;
	XQueryColor(xsp_disp, defcmap, &xcolor);
	XStoreColor(xsp_disp, xsp_cmap, &xcolor);
      }
    }

    for(c = 0; c < COLORNUM; c++) {
      xcolor.pixel = xsp_colors[c];
      xcolor.red = spscr_crgb[c].r << 10;
      xcolor.green = spscr_crgb[c].g << 10;
      xcolor.blue = spscr_crgb[c].b << 10;
      xcolor.flags = DoRed | DoGreen | DoBlue;
      XStoreColor(xsp_disp, xsp_cmap, &xcolor);
    }
    color_res = 1;
    currprivate = 1;
  }
  else {
    if(!firstalloced && currprivate) XFreeColormap(xsp_disp, xsp_cmap);
    xsp_cmap = defcmap;
    currprivate = 0;
  }
  firstalloced = 0;

  return color_res;
}


void spscr_refresh_colors(void)
{
  if(!allocate_colors()) put_msg("Could not allocate colors");
  
  kb_refresh_colormap();
  XSetWindowColormap(xsp_disp, xsp_win, xsp_cmap);
  XSetWindowBackground(xsp_disp, xsp_win, xsp_colors[7]);
  XInstallColormap(xsp_disp, xsp_cmap);
  spxs_init_color();
  sp_init_screen_mark();
}


#define MAX_RES_NAME 64
#define MAXLINELEN 512

void spcf_read_xresources(void)
{
  int i;
  char resname[MAX_RES_NAME], resclass[MAX_RES_NAME];
  char line[MAXLINELEN+1];
  char *val;

  resname[0] = '.';
  resclass[0] = '.';
  resname[MAX_RES_NAME-1] = '\0';
  resclass[MAX_RES_NAME-1] = '\0';
  
  for(i = 0; spcf_options[i].option != NULL; i++) {
    strncpy(resname+1, spcf_options[i].option, MAX_RES_NAME-2);
    strncpy(resclass+1, spcf_options[i].option, MAX_RES_NAME-2);
    resclass[1] = toupper(resclass[1]);

    val = aX_get_prog_res(resname, resclass);
    if(val != NULL) spcf_set_val(i, val, NULL, 0, 0);
  }
  
  strcpy(resname+1, "color");
  strcpy(resclass+1, "Color");

  for(i = 0; i < COLORNUM; i++) {
    sprintf(resname+6, "%i", i);
    sprintf(resclass+6, "%i", i);

    val = aX_get_prog_res(resname, resclass);
    if(val != NULL) spcf_set_color(i, val, NULL, 0, 0);
  }

  strcpy(resname+1, "keys");
  strcpy(resclass+1, "Keys");
  
  val = aX_get_prog_res(resname, resclass);
  if(val != NULL) {
    char *ival, *attr;
    int l;

    while(*val) {
      for(i = 0; val[i] && val[i] != ';'; i++);
      l = i;
      if(i > MAXLINELEN) i = MAXLINELEN;
      strncpy(line, val, (size_t) i);
      line[i] = '\0';
      
      if(spcf_parse_conf_line(line, &attr, &ival)) {
	int ix;
	
	ix = spcf_match_keydef(attr, "");
	if(ix >= 0) spcf_set_key(ix, ival, NULL, 0, 0);
      }
      
      if(!val[l]) break;
      val += (l+1);
    }
  }
}
