#!/usr/bin/make -f

tmp  = debian/tmp
ctmp = debian/spectemu-common
xtmp = debian/spectemu-x11

export DEB_CFLAGS_MAINT_APPEND := -Wall
export DEB_CXXFLAGS_MAINT_APPEND := -Wall

%:
	dh $@ --with autoreconf

autoreconf:
	autoreconf -f -i
	cp -f /usr/share/misc/config.guess /usr/share/misc/config.sub .

override_dh_autoreconf:
	dh_autoreconf debian/rules -- autoreconf

override_dh_auto_configure:
	dh_auto_configure -- --datadir=/etc --with-readline

override_dh_auto_build:
	$(MAKE) all tapeout
	$(MAKE) $(shell dpkg-buildflags --export=configure) -C utils

override_dh_auto_clean:
	[ ! -f Makefile ] || $(MAKE) realclean
	$(MAKE) -C utils clean

override_dh_auto_install:
	# datadir is deliberately different here because we're splitting it,
	# and this makes the split more convenient.
	dh_auto_install -- prefix=`pwd`/$(tmp)/usr \
			   datadir=`pwd`/$(tmp)/usr/share/spectemu

override_dh_install:
	mv -f $(tmp)/usr/share/spectemu/spectemu.cfg $(ctmp)/etc/spectemu/
	dh_install

	# dh_compress won't do these for us, as the package split causes
	# them to be broken symlinks.
	ln -sf spectemu.1.gz $(xtmp)/usr/share/man/man1/xspect.1.gz

override_dh_installdocs:
	dh_installdocs --link-doc=spectemu-common
