spectemu (0.94a-21) UNRELEASED; urgency=medium

  * Use debhelper-compat instead of debian/compat.

 -- Colin Watson <cjwatson@debian.org>  Sat, 03 Aug 2019 12:54:57 +0100

spectemu (0.94a-20) unstable; urgency=medium

  [ Colin Watson ]
  * Move VCS to salsa.debian.org.

  [ Ondřej Nový ]
  * d/control: Remove trailing whitespaces

 -- Colin Watson <cjwatson@debian.org>  Sat, 27 Oct 2018 16:32:39 +0100

spectemu (0.94a-19) unstable; urgency=medium

  * Update Vcs-Browser URL for alioth cgit.
  * Use HTTPS for Vcs-* URLs.
  * Use dh-exec to simplify override_dh_install target.

 -- Colin Watson <cjwatson@debian.org>  Sat, 31 Dec 2016 14:52:55 +0000

spectemu (0.94a-18) unstable; urgency=medium

  * Switch to git; adjust Vcs-* fields.
  * Add a couple more <stdlib.h> inclusions for exit().

 -- Colin Watson <cjwatson@debian.org>  Thu, 02 Jan 2014 06:37:16 +0000

spectemu (0.94a-17) unstable; urgency=low

  [ Colin Watson ]
  * Build-depend on debhelper (>= 9~) directly rather than requiring a
    Lintian override for use of 8.9.0.

  [ Moritz Muehlenhoff ]
  * Remove the spectemu-svga package, since svgalib is scheduled for removal
    (closes: #711115).

 -- Colin Watson <cjwatson@debian.org>  Fri, 07 Jun 2013 16:49:20 +0100

spectemu (0.94a-16) unstable; urgency=low

  * Use dh-autoreconf.
  * Remove config-guess-sub.patch in favour of updating config.guess and
    config.sub automatically at build time.  dh_autoreconf does not take
    care of that by default because spectemu does not use automake.

 -- Colin Watson <cjwatson@debian.org>  Sat, 09 Feb 2013 19:27:10 +0000

spectemu (0.94a-15) unstable; urgency=low

  * Fix passing of build flags to utils/Makefile.

 -- Colin Watson <cjwatson@debian.org>  Fri, 22 Jun 2012 14:06:14 +0100

spectemu (0.94a-14) unstable; urgency=low

  * Drop -ltermcap rather than unnecessarily build-depending on
    libncurses5-dev.  Thanks, Sven Joachim.

 -- Colin Watson <cjwatson@debian.org>  Fri, 21 Oct 2011 11:12:10 +0100

spectemu (0.94a-13) unstable; urgency=low

  * Explicitly build-depend on libncurses5-dev, since libreadline6-dev no
    longer depends on it.
  * Use dpkg-buildflags and debhelper 9 to set default compiler flags.

 -- Colin Watson <cjwatson@debian.org>  Thu, 20 Oct 2011 13:03:28 +0100

spectemu (0.94a-12) unstable; urgency=low

  * Use architecture restriction in spectemu-common's Suggests field rather
    than a substvar; this is sanctioned by policy 3.9.1.
  * Update Vcs-Bzr field for Alioth changes.

 -- Colin Watson <cjwatson@debian.org>  Sun, 24 Jul 2011 13:44:12 +0100

spectemu (0.94a-11) unstable; urgency=low

  * Convert to source format 3.0 (quilt) and DEP-3 patch tagging.

 -- Colin Watson <cjwatson@debian.org>  Sun, 09 May 2010 13:38:05 +0100

spectemu (0.94a-10) unstable; urgency=low

  * Build-depend on libreadline-dev, not libreadline5-dev (closes: #553856).
  * Use 'dh_installdocs --link-doc' option from debhelper 7.4.2.

 -- Colin Watson <cjwatson@debian.org>  Wed, 17 Feb 2010 14:23:29 +0000

spectemu (0.94a-9) unstable; urgency=low

  * Update DEB_BUILD_OPTIONS parsing code from policy 3.8.0.
  * Imported into a branch on bzr.debian.org; add Vcs-Bzr control field.
  * Convert to debhelper 7. I made extensive use of override targets, so
    this requires 7.0.50.
  * Build-depend on x11proto-core-dev rather than the transitional x-dev
    package.
  * Quote value of needs= in spectemu-svga.menu.
  * Update config.guess and config.sub from autotools-dev 20090611.1.

 -- Colin Watson <cjwatson@debian.org>  Mon, 07 Sep 2009 01:58:23 +0100

spectemu (0.94a-8) unstable; urgency=low

  * Build-depend on libsvga1-dev rather than svgalibg1-dev, and build
    spectemu-svga for amd64 too (closes: #483448).

 -- Colin Watson <cjwatson@debian.org>  Wed, 28 May 2008 22:04:57 +0100

spectemu (0.94a-7) unstable; urgency=low

  * Use autotools-dev's recommended ./configure options to set the host
    type.

 -- Colin Watson <cjwatson@debian.org>  Mon, 19 Nov 2007 08:27:23 +0000

spectemu (0.94a-6) unstable; urgency=low

  * Update to section structure from menu 2.1.35.
  * Capitalise title fields in menu files, add longtitle fields, and add
    "(SVGA)" to spectemu-svga's menu file.
  * Use the ${binary:Version} substvar rather than ${Source-Version}.
  * Don't ignore 'make clean' errors other than a missing Makefile.

 -- Colin Watson <cjwatson@debian.org>  Thu, 05 Jul 2007 12:34:13 +0100

spectemu (0.94a-5) unstable; urgency=low

  * Build-depend on x-dev, libx11-dev, libxt-dev, and libxext-dev rather
    than the transitional xlibs-dev package.
  * Use debhelper v4.

 -- Colin Watson <cjwatson@debian.org>  Fri, 16 Dec 2005 13:16:04 +0000

spectemu (0.94a-4) unstable; urgency=low

  * Build-depend on libreadline5-dev (closes: #326277).
  * Drop old HAVE_CONFIG_H workaround; bug #125398 was fixed a long time
    ago.
  * Update config.guess and config.sub from autotools-dev 20050803.1.
  * Include <stdlib.h> in spconf.c for exit().

 -- Colin Watson <cjwatson@debian.org>  Sat,  3 Sep 2005 23:04:13 +0100

spectemu (0.94a-3) unstable; urgency=low

  * Set CXX to g++, not gcc (closes: #270651).
  * Quote values in menu file.

 -- Colin Watson <cjwatson@debian.org>  Sat, 11 Sep 2004 18:43:38 +0100

spectemu (0.94a-2) unstable; urgency=low

  * Drop versioned fileutils dependency from before potato; coreutils
    provides that now anyway (closes: #175711).

 -- Colin Watson <cjwatson@debian.org>  Tue,  7 Jan 2003 20:25:29 +0000

spectemu (0.94a-1) unstable; urgency=low

  * It seems that the Spectrum ROM is non-free (can't sell, can't embed in
    hardware), although Amstrad don't mind them being redistributed. I've
    split the ROM into a separate spectrum-roms package, and this package is
    now in contrib rather than main (closes: #134261).
  * New repacked upstream source without the Spectrum ROM.
  * The ROM is now loaded from a file rather than being linked directly into
    the emulator executable (thanks, Philip Kendall).
  * Remove Emacs local variables from this changelog.
  * Remove /etc/spectemu on purge, depending on fileutils (>= 4.0-5) for the
    --ignore-fail-if-non-empty option.
  * Update config.guess and config.sub from autotools-dev 20020201.1.

 -- Colin Watson <cjwatson@debian.org>  Sun,  3 Mar 2002 23:10:22 +0000

spectemu (0.94-8) unstable; urgency=low

  * Correct capitalization of MHz in package descriptions (thanks, Matt
    Zimmerman; closes: #125373, #125374, #125375).
  * Evil hack to work around readline HAVE_CONFIG_H breakage (see bug
    #125398).

 -- Colin Watson <cjwatson@debian.org>  Tue, 18 Dec 2001 01:08:36 +0000

spectemu (0.94-7) unstable; urgency=low

  * Support cross-compilation the right way, with dpkg-architecture.

 -- Colin Watson <cjwatson@debian.org>  Mon,  8 Oct 2001 02:07:49 +0100

spectemu (0.94-6) unstable; urgency=low

  * debian/rules: Empty DH_OPTIONS in install target (closes: #112939).

 -- Colin Watson <cjwatson@debian.org>  Thu, 20 Sep 2001 19:32:39 +0100

spectemu (0.94-5) unstable; urgency=low

  * Revert dh_gencontrol hack now that debhelper is fixed, and build-depend
    on a fixed version.
  * Build-depend on libreadline4-dev as a real alternative for the virtual
    libreadline-dev.
  * Fix "Upstream Author(s)" dh_make-ism.
  * Policy version 3.5.6.

 -- Colin Watson <cjwatson@debian.org>  Sun, 16 Sep 2001 04:35:05 +0100

spectemu (0.94-4) unstable; urgency=low

  * Add to substvars using a documented interface this time, now that
    debhelper has reminded me that using undocumented ones is a bad idea.
  * Hacked around debhelper bug #89311.
  * Lose debian/dirs in favour of debian/spectemu-common.dirs.

 -- Colin Watson <cjwatson@debian.org>  Tue, 29 May 2001 18:50:58 +0100

spectemu (0.94-3) unstable; urgency=low

  * svgalib is no longer built for alpha.
  * Updated to policy version 3.5.2: no changes required.
  * Added lintian override for /usr/bin/vgaspect being setuid root.
  * Use debhelper's --same-arch flag more consistently.

 -- Colin Watson <cjwatson@debian.org>  Sat,  7 Apr 2001 20:48:31 +0100

spectemu (0.94-2) unstable; urgency=low

  * Taking over as maintainer, with John Travers' consent.
  * Updated to policy version 3.5.0:
    - FHS updates.
    - Build dependencies.
    - Support for DEB_BUILD_OPTIONS.
  * Use debhelper v3.
  * Call the source package spectemu, and build X11 and SVGAlib versions
    (closes: #37663). All the packages at least replace xspectemu, even
    spectemu-svga; the latter is to avoid problems on some architectures
    where vgaspect was accidentally included in the xspectemu package.
    spectemu-x11 conflicts with and provides xspectemu.
  * debian/rules: clean target now does so properly.
  * debian/copyright: Updated download URL and GPL pointer.
  * Install upstream changelog only once.
  * Removed README.DOS. :)

  * Build-depend on readline-dev so that readline support gets used
    (closes: #64626).
  * Use x-terminal-emulator rather than xterm in menu entry
    (closes: #59008).
  * Build tapeout utility (since vgaspect is now included in spectemu-svga,
    this closes: #45538).
  * Install configuration file in /etc/spectemu, and make it a conffile.
  * Build utils/* and install them in /usr/lib/spectemu. Add a note to
    README.Debian explaining why ('filt' and 'recs' would cause namespace
    pollution).

 -- Colin Watson <cjwatson@debian.org>  Fri, 16 Feb 2001 02:56:57 +0000

xspectemu (0.94-1) unstable; urgency=low

  * Initial Release.

 -- John Travers <jtravs@debian.org>  Mon, 15 Feb 1999 15:55:04 +0000
