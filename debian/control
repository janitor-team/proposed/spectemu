Source: spectemu
Section: contrib/otherosfs
Priority: optional
Maintainer: Colin Watson <cjwatson@debian.org>
Standards-Version: 3.5.6
Build-Depends: dpkg-dev (>= 1.16.1~), debhelper-compat (= 9), dh-exec, libreadline-dev, x11proto-core-dev, libx11-dev, libxt-dev, libxext-dev, dh-autoreconf, autotools-dev
Vcs-Git: https://salsa.debian.org/debian/spectemu.git
Vcs-Browser: https://salsa.debian.org/debian/spectemu

Package: spectemu-common
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: spectemu-x11
Replaces: xspectemu
Description: Fast 48k ZX Spectrum Emulator (common files)
 Spectemu emulates the 48k ZX Spectrum, which uses the Z80 microprocessor.
 This package contains common configuration files and utilities which are or
 can be used by either the X11 or the SVGAlib frontend.
 .
 It emulates the Z80 processor as well as the 48k Spectrum's other
 hardware: keyboard, screen, sound, tape I/O. The emulation is very
 close to the real thing, but it is still quite fast (It was reported
 to be working well on a laptop with 486 at 25MHz!). On the other hand,
 the user interface is not the best.
 .
 Features include:
    - Sound support through Linux kernel sound-card driver.
    - Snapshot saving and loading (.Z80 and .SNA format)
    - Tape emulation: loading from tape files (.TAP and .TZX format)
    - Optional quick loading of tapes.
    - Saving to tape files.
    - Separate utility to save tape files to real tape
    - Configurable with config files and from command line

Package: spectemu-x11
Architecture: any
Depends: spectemu-common (= ${binary:Version}), spectrum-roms, ${shlibs:Depends}, ${misc:Depends}
Conflicts: xspectemu
Replaces: xspectemu
Provides: xspectemu
Description: Fast 48k ZX Spectrum Emulator for X11
 xspect is the X11 version of Spectemu which emulates the 48k ZX
 Spectrum, which uses the Z80 microprocessor.
 .
 It emulates the Z80 processor as well as the 48k Spectrum's other
 hardware: keyboard, screen, sound, tape I/O. The emulation is very
 close to the real thing, but it is still quite fast (It was reported
 to be working well on a laptop with 486 at 25MHz!). On the other hand,
 the user interface is not the best.
 .
 Features include:
    - Sound support through Linux kernel sound-card driver.
    - Snapshot saving and loading (.Z80 and .SNA format)
    - Tape emulation: loading from tape files (.TAP and .TZX format)
    - Optional quick loading of tapes.
    - Saving to tape files.
    - Separate utility to save tape files to real tape
    - Configurable with config files and from command line
